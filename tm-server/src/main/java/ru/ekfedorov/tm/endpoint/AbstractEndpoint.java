package ru.ekfedorov.tm.endpoint;

import ru.ekfedorov.tm.api.service.ServiceLocator;

public abstract class AbstractEndpoint {

    final ServiceLocator serviceLocator;

    public AbstractEndpoint(final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}
