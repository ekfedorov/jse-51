package ru.ekfedorov.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.ekfedorov.tm.api.repository.model.IProjectGraphRepository;
import ru.ekfedorov.tm.api.repository.model.IUserGraphRepository;
import ru.ekfedorov.tm.api.service.IConnectionService;
import ru.ekfedorov.tm.api.service.model.IProjectGraphService;
import ru.ekfedorov.tm.enumerated.Sort;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.exception.empty.*;
import ru.ekfedorov.tm.exception.incorrect.IncorrectIndexException;
import ru.ekfedorov.tm.exception.system.NullComparatorException;
import ru.ekfedorov.tm.exception.system.NullObjectException;

import ru.ekfedorov.tm.model.ProjectGraph;
import ru.ekfedorov.tm.repository.model.ProjectGraphRepository;
import ru.ekfedorov.tm.repository.model.UserGraphRepository;


import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;
import static ru.ekfedorov.tm.util.ValidateUtil.notNullOrLessZero;

public final class ProjectGraphService extends AbstractGraphService<ProjectGraph> implements IProjectGraphService {

    public ProjectGraphService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    @SneakyThrows
    public void add(@Nullable final ProjectGraph entity) {
        if (entity == null) throw new NullObjectException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            projectRepository.add(entity);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable List<ProjectGraph> entities) {
        if (entities == null) throw new NullObjectException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            entities.forEach(projectRepository::add);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            projectRepository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean contains(@NotNull final String id) {
        if (isEmpty(id)) throw new IdIsEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            return projectRepository.findOneById(id).isPresent();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<ProjectGraph> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            return projectRepository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<ProjectGraph> findAllSort(
            @Nullable final String sort
    ) {
        if (isEmpty(sort)) throw new NullComparatorException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            @Nullable Sort sortType = Sort.valueOf(sort);
            @NotNull final Comparator<ProjectGraph> comparator = sortType.getComparator();
            return projectRepository.findAll().stream().sorted(comparator).collect(Collectors.toList());
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<ProjectGraph> findOneById(
            @Nullable final String id
    ) {
        if (isEmpty(id)) throw new IdIsEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            return projectRepository.findOneById(id);
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeOneById(
            @Nullable final String id
    ) {
        if (isEmpty(id)) throw new LoginIsEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            projectRepository.removeOneById(id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public ProjectGraph add(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        if (isEmpty(description)) throw new DescriptionIsEmptyException();
        @NotNull final ProjectGraph project = new ProjectGraph();
        project.setName(name);
        project.setDescription(description);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserGraphRepository userRepository = new UserGraphRepository(entityManager);
            project.setUser(userRepository.findOneById(userId).get());
            entityManager.getTransaction().begin();
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            projectRepository.add(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        if (status == null) throw new NullObjectException();
        @NotNull final Optional<ProjectGraph> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(status);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            projectRepository.update(entity.get());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void clear(@Nullable final String userId) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            projectRepository.clearByUserId(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public List<ProjectGraph> findAll(@Nullable final String userId) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            return projectRepository.findAllByUserId(userId);
        } finally {
            entityManager.close();
        }
    }


    @SneakyThrows
    @NotNull
    @Override
    public Optional<ProjectGraph> findOneById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            return projectRepository.findOneByIdAndUserId(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public Optional<ProjectGraph> findOneByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            return projectRepository.findOneByIndex(userId, index);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public Optional<ProjectGraph> findOneByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            return projectRepository.findOneByName(userId, name);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final ProjectGraph entity) {
        if (entity == null) throw new NullObjectException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            projectRepository.removeOneById(entity.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void remove(
            @Nullable final String userId, @Nullable final ProjectGraph entity
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (entity == null) throw new NullObjectException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            projectRepository.removeOneByIdAndUserId(userId, entity.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeOneById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            projectRepository.removeOneByIdAndUserId(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeOneByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            @NotNull Optional<ProjectGraph> project = projectRepository.findOneByIndex(userId, index);
            if (!project.isPresent()) throw new UserNotFoundException();
            projectRepository.removeOneByIdAndUserId(userId, project.get().getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeOneByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            projectRepository.removeOneByName(userId, name);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        @NotNull final Optional<ProjectGraph> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setName(name);
        entity.get().setDescription(description);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            projectRepository.update(entity.get());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        if (status == null) throw new NullObjectException();
        @NotNull final Optional<ProjectGraph> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(status);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            projectRepository.update(entity.get());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void changeStatusByName(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        if (status == null) throw new NullObjectException();
        @NotNull final Optional<ProjectGraph> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(status);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            projectRepository.update(entity.get());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public @NotNull List<ProjectGraph> findAll(@Nullable String userId, @Nullable String sort) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(sort)) throw new NullComparatorException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            @Nullable Sort sortType = Sort.valueOf(sort);
            @NotNull final Comparator<ProjectGraph> comparator = sortType.getComparator();
            return projectRepository
                    .findAllByUserId(userId)
                    .stream().sorted(comparator)
                    .collect(Collectors.toList());
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void finishById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        @NotNull final Optional<ProjectGraph> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.COMPLETE);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            projectRepository.update(entity.get());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void finishByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        @NotNull final Optional<ProjectGraph> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.COMPLETE);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            projectRepository.update(entity.get());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void finishByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        @NotNull final Optional<ProjectGraph> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.COMPLETE);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            projectRepository.update(entity.get());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void startById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        @NotNull final Optional<ProjectGraph> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.IN_PROGRESS);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            projectRepository.update(entity.get());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void startByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        @NotNull final Optional<ProjectGraph> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.IN_PROGRESS);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            projectRepository.update(entity.get());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void startByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) return;
        @NotNull final Optional<ProjectGraph> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.IN_PROGRESS);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            projectRepository.update(entity.get());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        @NotNull final Optional<ProjectGraph> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setName(name);
        entity.get().setDescription(description);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            projectRepository.update(entity.get());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
